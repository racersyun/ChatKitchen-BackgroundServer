﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Windows.Devices.Gpio;
using Windows.Foundation;

namespace ChatKitchenBackgroundServer
{
    class GasValve
    {
        private GpioPin servoMotorPin;
        private Stopwatch servoStopWatch;
        private double valvePos;
        private double valveClosePos = 1.1;
        private double valveOpenPos = 2;
        private double pulseFrequency = 20;

        private MLX90614 tempSensor;

        public GasValve()
        {
            valvePos = 0;
            servoStopWatch = Stopwatch.StartNew();
            GpioController controller = GpioController.GetDefault();
            servoMotorPin = controller.OpenPin(4);
            servoMotorPin.SetDriveMode(GpioPinDriveMode.Output);
            Windows.System.Threading.ThreadPool.RunAsync(this.MotorMoveThread, Windows.System.Threading.WorkItemPriority.High);
            CloseValve();

            tempSensor = new MLX90614();
            Windows.System.Threading.ThreadPool.RunAsync(this.TempEventCheckThread, Windows.System.Threading.WorkItemPriority.High);
        }

        public void OpenValve()
        {
            Globals.GB_VALVE_STATUS = true;
            valvePos = valveOpenPos;
        }

        public void CloseValve()
        {
            Globals.GB_VALVE_STATUS = false;
            valvePos = valveClosePos;
        }

        private void TempEventCheckThread(IAsyncAction action)
        {
            while (true)
            {
                try
                {
                    if (Globals.GB_VALVE_STATUS)
                    {
                        if (Convert.ToDouble(Globals.GB_TEMP_STATUS) > 33.0)
                        {
                            CloseValve();
                            Globals.GB_EVENT_STATUS = true;
                            Globals.GB_TEMP_EVENT = Globals.GB_TEMP_STATUS;
                        }
                        System.Threading.Tasks.Task.Delay(1000).Wait();
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }
            }
        }

        private void MotorMoveThread(IAsyncAction action)
        {
            while (true)
            {
                if (valvePos != 0)
                {
                    servoMotorPin.Write(GpioPinValue.High);
                }
                MotorMoveWait(valvePos);
                servoMotorPin.Write(GpioPinValue.Low);
                MotorMoveWait(pulseFrequency - valvePos);
            }
        }

        private void MotorMoveWait(double milliseconds)
        {
            long initialTick = servoStopWatch.ElapsedTicks;
            long initialElapsed = servoStopWatch.ElapsedMilliseconds;
            double desiredTicks = milliseconds / 1000.0 * Stopwatch.Frequency;
            double finalTick = initialTick + desiredTicks;
            while (servoStopWatch.ElapsedTicks < finalTick) ;
        }
    }
}