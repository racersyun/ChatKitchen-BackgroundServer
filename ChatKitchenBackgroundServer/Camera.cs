﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using System.Net;
using Windows.UI.Xaml.Controls;
using System.Diagnostics;
using Windows.Foundation;

namespace ChatKitchenBackgroundServer
{
    class Camera
    {
        private MediaCapture mediaCapture;
        private StorageFile photoFile;
        private bool isPreviewing;
        private String filename;
        private KidDetect kd;

        private const int REQ_PHOTO_NORMAL = 1;
        private const int REQ_PHOTO_EVENT = 2;

        public Camera()
        {
            isPreviewing = false;
            CameraInit();
            Windows.System.Threading.ThreadPool.RunAsync(this.takeKidPhoto, Windows.System.Threading.WorkItemPriority.High);
        }
        enum Action
        {
            ENABLE,
            DISABLE
        }

        public void takephoto()
        {
            takePhoto(REQ_PHOTO_NORMAL);
            System.Threading.Tasks.Task.Delay(1000).Wait();
        }

        private void takeKidPhoto(IAsyncAction action)
        {
            while (true)
            {
                if (Globals.GB_VALVE_STATUS)
                {
                    takePhoto(REQ_PHOTO_EVENT);
                    System.Threading.Tasks.Task.Delay(1000).Wait();
                    kd = new KidDetect("child.jpg");
                }
                System.Threading.Tasks.Task.Delay(10 * 1000).Wait();
            }
        }

        private async void Cleanup()
        {
            if (mediaCapture != null)
            {
                // Cleanup MediaCapture object
                if (isPreviewing)
                {
                    await mediaCapture.StopPreviewAsync();
                    isPreviewing = false;
                }
                mediaCapture.Dispose();
                mediaCapture = null;
            }
        }

        private CaptureElement previewElement;

        private async Task SetPhotoResolution()
        {
            var resolutions = mediaCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.Photo).Select(x => x as VideoEncodingProperties);
            var maxRes = resolutions.OrderByDescending(x => x.Height * x.Width).FirstOrDefault();
            await mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.Photo, maxRes);
        }

        private async void CameraInit()
        {
            previewElement = new CaptureElement();
            try
            {
                if (mediaCapture != null)
                {
                    if (isPreviewing)
                    {
                        await mediaCapture.StopPreviewAsync();
                        previewElement.Source = null;
                        isPreviewing = false;
                    }
                    mediaCapture.Dispose();
                    mediaCapture = null;
                }
                //init
                mediaCapture = new MediaCapture();
                await mediaCapture.InitializeAsync();
                await SetPhotoResolution();

                //preview start
                previewElement.Source = mediaCapture;
                await mediaCapture.StartPreviewAsync();
                isPreviewing = true;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        //사진찍기
        private async void takePhoto(int req)
        {
            try
            {
                if (isPreviewing)
                {
                    String nowTime = DateTime.Now.ToString();
                    if (req == REQ_PHOTO_NORMAL)
                    {
                        filename = nowTime + ".jpg";
                        filename = filename.Replace(':', '-');
                    }
                    else if (req == REQ_PHOTO_EVENT)
                    {
                        filename = "child.jpg";
                    }
                    photoFile = await KnownFolders.PicturesLibrary.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                    ImageEncodingProperties imageProperties = ImageEncodingProperties.CreateJpeg();
                    await mediaCapture.CapturePhotoToStorageFileAsync(imageProperties, photoFile);
                    Globals.GB_PHOTO_NAME = filename;
                }
            }
            catch (Exception e)
            {
                Cleanup();
                Debug.WriteLine(e.ToString());
            }
        }
    }
}
