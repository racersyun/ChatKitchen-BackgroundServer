﻿// 
// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license.
// 
// Microsoft Cognitive Services (formerly Project Oxford): https://www.microsoft.com/cognitive-services
// 
// Microsoft Cognitive Services (formerly Project Oxford) GitHub:
// https://github.com/Microsoft/Cognitive-Vision-Windows
// 
// Copyright (c) Microsoft Corporation
// All rights reserved.
// 
// MIT License:
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED ""AS IS"", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Vision.Contract;
using Windows.Storage;

namespace ChatKitchenBackgroundServer
{
    class KidDetect
    {
        public KidDetect() { }
        public KidDetect(string imgPath)
        {
            DoWork("C:\\Data\\Users\\DefaultAccount\\Pictures\\" + imgPath, true);
        }

        private async Task DoWork(string imageUri, bool upload)
        {
            AnalysisResult analysisResult;
            analysisResult = await UploadAndAnalyzeImage(imageUri);

            LogAnalysisResult(analysisResult);
        }

        private async Task<AnalysisResult> UploadAndAnalyzeImage(string imageFilePath)
        {
            VisionServiceClient VisionServiceClient = new VisionServiceClient("PASTE YOUR COGNITIVE VISION API KEY HERE");

            using (Stream imageFileStream = File.OpenRead(imageFilePath))
            {
                VisualFeature[] visualFeatures = new VisualFeature[] { VisualFeature.Description, VisualFeature.Faces, VisualFeature.Tags };
                AnalysisResult analysisResult = await VisionServiceClient.AnalyzeImageAsync(imageFileStream, visualFeatures);
                return analysisResult;
            }
        }

        private void LogAnalysisResult(AnalysisResult result)
        {
            Globals.GB_KID_APPROACH = false;
            Globals.GB_GLS_APPROACH = false;
            if (result == null) { return; }

            if (result.Faces != null && result.Faces.Length > 0)
            {
                foreach (var face in result.Faces)
                {
                    if (face.Age < 10) { Globals.GB_KID_APPROACH = true; }
                }
            }

            if (result.Description != null)
            {
                string tags = "Tags : ";
                foreach (var tag in result.Description.Tags)
                {
                    tags += tag + ", ";
                }

                if (tags.Contains("baby") || tags.Contains("child") ||
                    tags.Contains("boy") || tags.Contains("girl"))
                { Globals.GB_KID_APPROACH = true; }

                if (tags.Contains("glasses"))
                { Globals.GB_GLS_APPROACH = true; }
            }
        }
    }
}
