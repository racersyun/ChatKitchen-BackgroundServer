﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.Storage;
using System.Diagnostics;

namespace ChatKitchenBackgroundServer
{
    class WebServer
    {
        private StreamSocketListener listener;
        private const uint BufferSize = 8192;
        private GasValve ct;
        private Camera cam;

        public WebServer()
        {
            ct = new GasValve();
            cam = new Camera();
        }

        public void InitServer()
        {
            listener = new StreamSocketListener();
            listener.BindServiceNameAsync("PUT YOUR PORT NUMBER HERE");
            listener.ConnectionReceived += async (sender, args) =>
            {
                HandleRequest(sender, args);
            };
        }

        public async void HandleRequest(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
        {
            StringBuilder request = new StringBuilder();

            using (IInputStream input = args.Socket.InputStream)
            {
                byte[] data = new byte[BufferSize];
                IBuffer buffer = data.AsBuffer();
                uint dataRead = BufferSize;

                while (dataRead == BufferSize)
                {
                    await input.ReadAsync(buffer, BufferSize, InputStreamOptions.Partial);
                    request.Append(Encoding.UTF8.GetString(data, 0, data.Length));
                    dataRead = buffer.Length;
                }
            }

            using (IOutputStream output = args.Socket.OutputStream)
            {
                using (Stream response = output.AsStreamForWrite())
                {
                    byte[] bodyArray = await ParseRequest(request.ToString());

                    var bodyStream = new MemoryStream(bodyArray);
                    var header = "HTTP/1.1 200 OK\r\n" +
                                $"Content-Length: {bodyStream.Length}\r\n" +
                                    "Connection: close\r\n\r\n";

                    byte[] headerArray = Encoding.UTF8.GetBytes(header);

                    try
                    {
                        await response.WriteAsync(headerArray, 0, headerArray.Length);
                        await bodyStream.CopyToAsync(response);
                        await response.FlushAsync();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.ToString());
                    }
                }
            }
        }

        private async Task<byte[]> ParseRequest(string req)
        {
            char sp = '?';
            string[] result = req.Split(sp);
            string resmsg = "";
            byte[] resbyte;

            try
            {
                switch (result[1])
                {
                    case "checkTemp":
                        resbyte = Encoding.UTF8.GetBytes("<html><body>" + Globals.GB_TEMP_STATUS + "</body></html>");
                        break;
                    case "statusValve":
                        if (Globals.GB_VALVE_STATUS) resmsg = "open";
                        else resmsg = "close";
                        resbyte = Encoding.UTF8.GetBytes("<html><body>" + resmsg + "</body></html>");
                        break;
                    case "closeValve":
                        ct.CloseValve();
                        resbyte = new byte[1];
                        break;
                    case "openValve":
                        ct.OpenValve();
                        resbyte = new byte[1];
                        break;
                    case "checkImage":
                        cam.takephoto();
                        resbyte = await ReadFile(Globals.GB_PHOTO_NAME);
                        break;
                    case "checkEvent":
                        if (Globals.GB_EVENT_STATUS) { resmsg = "gas_temp" + "<<" + Globals.GB_TEMP_EVENT + ">>"; }
                        resbyte = Encoding.UTF8.GetBytes("<html><body>" + resmsg + "</body></html>");
                        Globals.GB_EVENT_STATUS = false;
                        break;
                    case "checkChild":
                        if (Globals.GB_KID_APPROACH)
                        {
                            resmsg = "kid";
                            ct.CloseValve();
                        }
                        if (Globals.GB_GLS_APPROACH)
                        {
                            resmsg = "glasses";
                            ct.CloseValve();
                        }
                        if (!Globals.GB_GLS_APPROACH && !Globals.GB_KID_APPROACH) { resmsg = "none"; }
                        resbyte = Encoding.UTF8.GetBytes("<html><body>" + resmsg + "</body></html>");
                        Globals.GB_KID_APPROACH = false;
                        Globals.GB_GLS_APPROACH = false;                        
                        break;
                    case "checkChildPhoto":
                        resbyte = await ReadFile("child.jpg");
                        StorageFolder imageFolder = KnownFolders.PicturesLibrary;
                        await imageFolder.GetFileAsync("child.jpg");
                        break;
                    default:
                        resmsg = result[0];
                        resbyte = new byte[1];
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                resbyte = new byte[1];
            }

            return resbyte;
        }

        public async Task<byte[]> ReadFile(string filename)
        {
            StorageFolder imageFolder = KnownFolders.PicturesLibrary;
            StorageFile imageFile = await imageFolder.GetFileAsync(filename);
            byte[] fileBytes = null;
            using (IRandomAccessStreamWithContentType stream = await imageFile.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (DataReader reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }

            return fileBytes;
        }
    }
}
