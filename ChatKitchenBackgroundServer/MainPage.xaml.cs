﻿using System;
using System.Collections.Generic;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


// 빈 페이지 항목 템플릿에 대한 설명은 https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x412에 나와 있습니다.

namespace ChatKitchenBackgroundServer
{
    public sealed partial class MainPage : Page
    {
        private WebServer mServer;

        public MainPage()
        {
            this.InitializeComponent();
            
            mServer = new WebServer();
            mServer.InitServer();
        }

        private void Click_Refresh(object sender, RoutedEventArgs arg)
        {
        }

        private void Click_ValveChg(object sender, RoutedEventArgs arg)
        {
        }        
    }
}
