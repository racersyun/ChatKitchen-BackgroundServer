﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatKitchenBackgroundServer
{
    class Globals
    {
        public static string GB_PHOTO_NAME = "";
        public static bool GB_EVENT_STATUS = false;
        public static string GB_TEMP_STATUS = "0";
        public static string GB_TEMP_EVENT = "0";
        public static bool GB_VALVE_STATUS = false;
        public static bool GB_KID_APPROACH = false;
        public static bool GB_GLS_APPROACH = false;
    }
}
