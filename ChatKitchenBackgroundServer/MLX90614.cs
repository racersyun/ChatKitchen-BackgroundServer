﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Windows.Devices.I2c;
using Windows.Devices.Enumeration;
using Windows.UI.Xaml;


namespace ChatKitchenBackgroundServer
{
    class MLX90614
    {
        private const byte MLX90614_I2CADDR = 0x5A;
        private const byte MLX90614_TOBJ1 = 0x07;   // Object Temp

        private I2cDevice mlx90614sensor;
        private DispatcherTimer timer;

        private double currentTemp = 0;

        public MLX90614()
        {
            StartScenarioAsync();
        }

        public string ReturnTemp()
        {
            return currentTemp.ToString();
        }

        private async Task StartScenarioAsync()
        {
            string i2cDeviceSelector = I2cDevice.GetDeviceSelector();
            IReadOnlyList<DeviceInformation> devices = await DeviceInformation.FindAllAsync(i2cDeviceSelector);

            var MLX90614_settings = new I2cConnectionSettings(MLX90614_I2CADDR);

            mlx90614sensor = await I2cDevice.FromIdAsync(devices[0].Id, MLX90614_settings);

            timer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(500) };
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, object e)
        {
            var command = new byte[1];
            var temperatureData = new byte[2];

            command[0] = MLX90614_TOBJ1;
            mlx90614sensor.WriteRead(command, temperatureData);

            var rawTempReading = ((temperatureData[1] & 0x007F) << 8) + temperatureData[0];
            currentTemp = (rawTempReading * 0.02) - 273.15;

            Globals.GB_TEMP_STATUS = string.Format("{0:0.00}", currentTemp);
        }
    }
}
